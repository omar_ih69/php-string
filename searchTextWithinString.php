<!DOCTYPE html>
<html>
<body>

<?php
echo strpos("Hello world!", "world");
?>

<p>The PHP strpos() function searches for a specific text within a string.

If a match is found, the function returns the character position of the first match. If no match is found, it will return FALSE.

The example below searches for the text "world" in the string "Hello world!"</p>
</body>
</html>